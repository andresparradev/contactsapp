import React from 'react';
import { Link } from 'wouter';
import './index.css';
import { Button } from 'react-bootstrap';
import Logo from '../../components/logo';

function Home() {
  return <div className="Home">
    <div className="Home__content">
      <Logo />
      <div className="Home__buttons">
        <Link to="/login"><Button variant="dark" block>Iniciar secion</Button></Link>
        <Link to="/register"><Button variant="dark" block>Registrase</Button></Link>
      </div>
    </div>
  </div>
}

export default Home;
