import React from 'react';
import './index.css';
import {
  Form,
  Button
} from 'react-bootstrap';
import Logo from '../../components/logo';

function Login() {
  return (
    <div className="Login">
      <section className="Login__form">
        <Logo center />
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Enter password" />
          </Form.Group>

          <Button variant="success" block>Login</Button>
        </Form>
      </section>
    </div>
  );
}

export default Login;
