import React from 'react';
import './index.css';
import {
  Form,
  Button
} from 'react-bootstrap';
import Logo from '../../components/logo';

function Register() {
  return (
    <div className="Register">
      <section className="Register__form">
        <Logo center />
        <Form>
          <Form.Group controlId="formBasicName">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="Enter name" />
          </Form.Group>
          
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Enter password" />
          </Form.Group>

          <Form.Group controlId="formBasicConfirmPassword">
            <Form.Label>Confirm password</Form.Label>
            <Form.Control type="password" placeholder="Enter confirm password" />
          </Form.Group>

          <Button variant="success" block>Register</Button>
        </Form>
      </section>
    </div>
  );
}

export default Register;
