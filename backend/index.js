const express = require('express');
const path = require('path');
const cors = require('cors');
const app = express();

app.set('PORT', process.env.NODE_ENV || 9000);
app.use(cors());
app.use(express.json());

app.get(['/', '/login', '/register'], (req, res) => {
  res.sendFile('index.html', {
    root: path.resolve(__dirname, 'public')
  });
});

app.get('/api', (req, res) => {
  res.send('Hello!');
});

app.use(express.static(path.resolve(__dirname, 'public')));

app.listen(app.get('PORT'), (err) => console.log(err ? 'Error' : 'OK :)'));